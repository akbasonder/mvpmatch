import * as React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import Dashboard from './components/Dashboard';
import theme from './theme';
import DateAdapter from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
    <CssBaseline />
    <LocalizationProvider dateAdapter={DateAdapter}>
      <Dashboard />
    </LocalizationProvider>
  </ThemeProvider>,
  document.querySelector('#root'),
);
