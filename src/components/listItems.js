import * as React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PeopleIcon from '@mui/icons-material/People';
import BarChartIcon from '@mui/icons-material/BarChart';
import LayersIcon from '@mui/icons-material/Layers';

const SCALE_WIDTH = screen.width/1440

export const MainListItems = (
  <div>
    <ListItem button style={{marginTop:41, marginLeft:18*SCALE_WIDTH}}>
      <ListItemIcon>
        <DashboardIcon style={{width:30, height:30}}/>
      </ListItemIcon>
    </ListItem>
    <ListItem button style={{marginTop:5, marginLeft:18*SCALE_WIDTH}}>
      <ListItemIcon>
        <ShoppingCartIcon style={{width:30, height:30}}/>
      </ListItemIcon>
    </ListItem>
    <ListItem button style={{marginTop:5, marginLeft:18*SCALE_WIDTH}}>
      <ListItemIcon>
        <PeopleIcon style={{width:30, height:30}}/>
      </ListItemIcon>
    </ListItem>
    <ListItem button style={{marginTop:5, marginLeft:18*SCALE_WIDTH}}>
      <ListItemIcon>
        <BarChartIcon style={{width:30, height:30}}/>
      </ListItemIcon>
    </ListItem>
    <ListItem button style={{marginTop:5, marginLeft:18*SCALE_WIDTH}}>
      <ListItemIcon>
        <LayersIcon style={{width:30, height:30}}/>
      </ListItemIcon>
    </ListItem>
  </div>
);