import * as React from "react";
import { styled, createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import MuiDrawer from "@mui/material/Drawer";
import Box from "@mui/material/Box";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import MenuIcon from "@mui/icons-material/Menu";
import { MainListItems } from "./ListItems";
import { BasicSelect } from "./Filters";
import { fetchGateways, fetchProjects, fetchReports } from "../services";
import DatePicker from "@mui/lab/DatePicker";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { format } from "date-fns";
import Report from "./Report";

const SCALE_WIDTH = screen.width / 1440;
const drawerWidth = 100 * SCALE_WIDTH; //240;

const AppBar = styled(MuiAppBar)(({ theme }) => ({
  zIndex: theme.zIndex.drawer + 1,
  backgroundColor: "white",
  height: 80,
  justifyContent: "center",
  borderBottomColor: "grey",
  borderBottomWidth: 1,
}));

const Drawer = styled(MuiDrawer)(({ theme }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    boxSizing: "border-box",
    overflowX: "hidden",
    //width: theme.spacing(8),
  },
}));

const mdTheme = createTheme();

export const AllFilterDataContext = React.createContext({
  projects: [],
  gateways: [],
});

function DashboardContent() {
  const [projects, setProjects] = React.useState([]);
  const [gateways, setGateways] = React.useState([]);
  React.useEffect(() => {
    fetchProjects()
      .then((res) => res.json())
      .then((res) => {
        const allProjects = res.data;
        allProjects.splice(0, 0, { projectId: "All", name: "All Projects" });
        setProjects(allProjects);
      })
      .catch((err) => console.log("fetchProjects err", err));

    fetchGateways()
      .then((res) => res.json())
      .then((res) => {
        const allGateways = res.data;
        allGateways.splice(0, 0, { gatewayId: "All", name: "All Gateways" });
        setGateways(allGateways);
      })
      .catch((err) => console.log("fetchProjects err", err));
  }, [setProjects, setGateways]);

  const [projectId, setProjectId] = React.useState("All");
  const [gatewayId, setGatewayId] = React.useState("All");
  const [fromDate, setFromDate] = React.useState(new Date());
  const [toDate, setToDate] = React.useState(new Date());
  const [reports, setReports] = React.useState(undefined);

  const generateReport = React.useCallback(() => {
    fetchReports(
      format(fromDate, "yyyy-MM-dd"),
      format(toDate, "yyyy-MM-dd"),
      projectId === "All" ? undefined : projectId,
      gatewayId === "All" ? undefined : gatewayId
    )
      .then((res) => res.json())
      .then((res) => {
        setReports(res.data);
      })
      .catch((err) => console.log("fetchProjects err", err));
  }, [projectId, gatewayId, fromDate, toDate, setReports]);

  return (
    <ThemeProvider theme={mdTheme}>
      <AllFilterDataContext.Provider value={{ projects, gateways }}>
        <Box sx={{ display: "flex" }}>
          <CssBaseline />
          <AppBar position="absolute">
            <Toolbar>
              <Typography
                component="h1"
                variant="h6"
                color="inherit"
                noWrap
                sx={{ flexGrow: 1, color: "#00628B" }}
              >
                B
                <MenuIcon style={{ color: "red" }} />
              </Typography>

              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginRight: 70 * SCALE_WIDTH,
                }}
              >
                <Box
                  style={{
                    display: "flex",
                    width: 43,
                    height: 43,
                    backgroundColor: "#F6CA65",
                    borderRadius: 5,
                    justifyContent: "center",
                    alignItems: "center",
                    fontSize: 23,
                    marginRight: 11 * SCALE_WIDTH,
                  }}
                >
                  JD
                </Box>
                <Box
                  style={{
                    color: "#005B96",
                    fontSize: 16,
                    marginRight: 30 * SCALE_WIDTH,
                  }}
                >
                  John Doe
                </Box>
              </Box>
            </Toolbar>
          </AppBar>
          <Drawer variant="permanent">
            <Toolbar
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
                px: [1],
              }}
            ></Toolbar>
            <List>{MainListItems}</List>
          </Drawer>
          <Box
            component="main"
            sx={{
              flexGrow: 1,
              height: "100vh",
              overflow: "auto",
            }}
          >
            <Toolbar />

            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: 35 * SCALE_WIDTH,
              }}
            >
              <Box
                style={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <Box style={{ width: 265 * SCALE_WIDTH }}>
                  <Box
                    style={{
                      fontSize: 24 * SCALE_WIDTH,
                      color: "#011F4B",
                      fontWeight: "bold",
                    }}
                  >
                    Reports
                  </Box>
                  <Box
                    style={{
                      fontSize: 16,
                      color: "#7E8299",
                      fontWeight: "bold",
                    }}
                  >
                    Easily generate a report of your transactions
                  </Box>
                </Box>
                <Box style={{ marginLeft: 196 * SCALE_WIDTH }}>
                  <BasicSelect
                    options={projects}
                    labelField="name"
                    valueField="projectId"
                    value={projectId}
                    setValue={setProjectId}
                    width={135 * SCALE_WIDTH}
                  />
                </Box>
                <Box style={{ marginLeft: 23 * SCALE_WIDTH }}>
                  <BasicSelect
                    options={gateways}
                    labelField="name"
                    valueField="gatewayId"
                    value={gatewayId}
                    setValue={setGatewayId}
                    width={145 * SCALE_WIDTH}
                  />
                </Box>
                <Box
                  style={{
                    marginLeft: 23 * SCALE_WIDTH,
                    height: 32 * SCALE_WIDTH,
                    width: 118 * SCALE_WIDTH,
                  }}
                >
                  <DatePicker
                    label="From date"
                    value={fromDate}
                    onChange={setFromDate}
                    renderInput={(params) => (
                      <TextField
                        sx={{
                          //svg: { color:'#1BC5BD',},
                          //input: { backgroundColor:'#1BC5BD',color:'white', height:10 },
                          //label: { color:'white', marginTop:0.5},
                          input: { height: 10, width: 118 },
                        }}
                        {...params}
                      />
                    )}
                  />
                </Box>
                <Box
                  style={{
                    marginLeft: 36 * SCALE_WIDTH,
                    width: 118 * SCALE_WIDTH,
                  }}
                >
                  <DatePicker
                    InputProps={{}}
                    label="To date"
                    value={toDate}
                    onChange={setToDate}
                    renderInput={(params) => (
                      <TextField
                        sx={{
                          input: { height: 10, width: 118 },
                        }}
                        {...params}
                      />
                    )}
                  />
                </Box>
                <Button
                  variant="contained"
                  onClick={generateReport}
                  style={{
                    marginLeft: 36 * SCALE_WIDTH,
                    height: 32 * SCALE_WIDTH,
                    width: 118 * SCALE_WIDTH,
                    fontSize: 13,
                  }}
                >
                  Generate Report
                </Button>
              </Box>
            </Box>

            <Box style={{ marginRight: 100 * SCALE_WIDTH }}>
              <Report
                projectId={projectId}
                gatewayId={gatewayId}
                reports={reports}
              />
            </Box>
          </Box>
        </Box>
      </AllFilterDataContext.Provider>
    </ThemeProvider>
  );
}

export default function Dashboard() {
  return <DashboardContent />;
}
