import * as React from "react";
import Link from "@mui/material/Link";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import Box from "@mui/material/Box";
import { Doughnut } from "react-chartjs-2";
import { Card, CardContent, CardHeader, Divider } from "@mui/material";
import { Chart, registerables, ArcElement } from "chart.js";
import { AllFilterDataContext } from "./Dashboard";
import NumberFormat from "react-number-format";
Chart.register(...registerables);
Chart.register(ArcElement);

const CASES = {
  ALL_PROJECTS_ALL_GATEWAYS: 1,
  ONE_PROJECT_ALL_GATEWAYS: 2,
  ALL_PROJECTS_ONE_GATEWAY: 3,
  ONE_PROJECT_ONE_GATEWAY: 4,
};

const options = {
  animation: false,
  cutoutPercentage: 80,
  layout: { padding: 0 },
  legend: {
    display: false,
  },
  maintainAspectRatio: false,
  responsive: true,
  rotation: 120,
  tooltips: {
    backgroundColor: "black",
    bodyFontColor: "black",
    borderColor: "black",
    borderWidth: 1,
    enabled: true,
    footerFontColor: "black",
    intersect: false,
    mode: "index",
    titleFontColor: "black",
  },
};

const SCALE_WIDTH = screen.width / 1440;
const SCALE_HEIGHT = screen.height / 900;

export default function Report({ projectId, gatewayId, reports }) {
  let selectedCase = CASES.ONE_PROJECT_ONE_GATEWAY,
    field = "",
    sums = [];

  if (projectId === "All" && gatewayId === "All") {
    field = "projectId";
    selectedCase = CASES.ALL_PROJECTS_ALL_GATEWAYS;
  } else if (projectId !== "All" && gatewayId === "All") {
    field = "gatewayId";
    selectedCase = CASES.ONE_PROJECT_ALL_GATEWAYS;
  } else if (projectId === "All" && gatewayId !== "All") {
    field = "projectId";
    selectedCase = CASES.ALL_PROJECTS_ONE_GATEWAY;
  }
  const reportsWithSum = sort(field, reports, sums);
  const showGraph =
    selectedCase === CASES.ONE_PROJECT_ALL_GATEWAYS ||
    selectedCase === CASES.ALL_PROJECTS_ONE_GATEWAY;

  return (
    <React.Fragment>
      {reports === undefined ? (
        <NoReport />
      ) : (
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            marginTop: 27 * SCALE_WIDTH,
          }}
        >
          <Box style={{ width: showGraph ? `56%` : "100%" }}>
            <Sheet
              projectId={projectId}
              gatewayId={gatewayId}
              selectedCase={selectedCase}
              reports={reportsWithSum}
            />
          </Box>
          {showGraph && <Graph selectedCase={selectedCase} sums={sums} />}
        </Box>
      )}
    </React.Fragment>
  );
}

function NoReport() {
  return (
    <Box
      style={{
        display: "flex",
        width: "100%",
        height: screen.height * 0.6,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Box fontSize={24}>No reports</Box>

      <Box style={{ width: 470 }} fontSize={16}>
        Currently you have no data for the reports to be generated. Once you
        start generating traffic through the Balance application the reports
        will be shown
      </Box>
    </Box>
  );
}

function Sheet({ projectId, gatewayId, selectedCase, reports = [] }) {
  const { projects, gateways } = React.useContext(AllFilterDataContext);

  const gatewayName = getNameByField(gateways, "gatewayId", gatewayId);
  const projectName = getNameByField(projects, "projectId", projectId);
  const showGraph =
    selectedCase === CASES.ONE_PROJECT_ALL_GATEWAYS ||
    selectedCase === CASES.ALL_PROJECTS_ONE_GATEWAY;
  let rowBackground = "white";
  return (
    <Box
      style={{
        backgroundColor: "#F1FAFE",
        marginRight: showGraph ? 15 * SCALE_WIDTH : 27 * SCALE_WIDTH,
      }}
    >
      <Box
        style={{ marginLeft: 24 * SCALE_WIDTH, marginRight: 24 * SCALE_WIDTH }}
      >
        <Box style={{ fontSize: 16, fontWeight: "bold", height: 71 }}>
          {projectName} | {gatewayName}{" "}
        </Box>
        <Table size="small" style={{ tableLayout: "fixed", marginBottom: 17 }}>
          <TableBody>
            {reports.map((row, index) => {
              if (row.totalSum) {
                return <Box />;
              }
              const oneProjectAllGateways =
                selectedCase === CASES.ONE_PROJECT_ALL_GATEWAYS;
              rowBackground =
                (index > 0 && reports[index - 1].sum) ||
                row.sum ||
                rowBackground === "#F1FAFE"
                  ? "white"
                  : "#F1FAFE";
              return row.sum ? (
                selectedCase === CASES.ONE_PROJECT_ONE_GATEWAY ? (
                  <Box />
                ) : (
                  <TableRow
                    sx={{
                      "& td": { borderTop: 5, borderTopColor: "#F1FAFE" },
                      backgroundColor: "white",
                      height: 71,
                    }}
                  >
                    <TableCell>
                      <Box style={{ fontWeight: "bold" }}>
                        {getNameByField(
                          oneProjectAllGateways ? gateways : projects,
                          oneProjectAllGateways ? "gatewayId" : "projectId",
                          row.id
                        )}
                      </Box>
                    </TableCell>
                    {selectedCase === CASES.ALL_PROJECTS_ALL_GATEWAYS && (
                      <TableCell />
                    )}
                    <TableCell />
                    <TableCell align="right">
                      <Box style={{ fontWeight: "bold" }}>
                        <ShowAmount amount={row.sum} />
                      </Box>
                    </TableCell>
                  </TableRow>
                )
              ) : (
                <>
                  {index > 0 && reports[index - 1].sum && (
                    <TableRow
                      key={row.paymentId}
                      sx={{
                        "& td": { borderTop: 14, borderTopColor: "#F1FAFE" },
                        backgroundColor: "white",
                        height: 35,
                      }}
                    >
                      <TableCell>Date</TableCell>
                      {selectedCase === CASES.ALL_PROJECTS_ALL_GATEWAYS && (
                        <TableCell>Gateway Id</TableCell>
                      )}
                      <TableCell>Transaction ID</TableCell>
                      <TableCell align="right">Amount</TableCell>
                    </TableRow>
                  )}

                  <TableRow
                    key={row.paymentId}
                    sx={{
                      "& td": {
                        borderTop: 5,
                        borderBottom: 0,
                        borderTopColor: "#F1FAFE",
                      },
                      backgroundColor: rowBackground,
                      height: 35,
                    }}
                  >
                    <TableCell>{row.created}</TableCell>
                    {selectedCase === CASES.ALL_PROJECTS_ALL_GATEWAYS && (
                      <TableCell>
                        {getNameByField(gateways, "gatewayId", row.gatewayId)}
                      </TableCell>
                    )}
                    <TableCell>{row.paymentId}</TableCell>
                    <TableCell align="right">
                      <ShowAmount amount={row.amount} />
                    </TableCell>
                  </TableRow>
                </>
              );
            })}
          </TableBody>
        </Table>
      </Box>
      <Box>
        {(selectedCase === CASES.ALL_PROJECTS_ALL_GATEWAYS ||
          selectedCase === CASES.ONE_PROJECT_ONE_GATEWAY) && (
          <Box
            style={{
              backgroundColor: "#F1FAFE",
              fontWeight: "bold",
              marginTop: 27,
              marginLeft: 24 * SCALE_WIDTH,
              height: 53,
            }}
          >
            Total:
            <ShowAmount amount={reports[reports.length - 1].totalSum} />
          </Box>
        )}
      </Box>
    </Box>
  );
}

function ShowAmount({ amount }) {
  return (
    <NumberFormat
      thousandsGroupStyle="thousand"
      value={amount}
      suffix=" USD"
      decimalSeparator="."
      displayType="text"
      type="text"
      thousandSeparator={true}
      allowNegative={false}
      decimalScale={2}
    />
  );
}

function Graph({ selectedCase, sums = [] }) {
  const { projects, gateways } = React.useContext(AllFilterDataContext);
  const values = [];
  const labels = [];
  const names =
    selectedCase === CASES.ONE_PROJECT_ALL_GATEWAYS ? gateways : projects;
  const field =
    selectedCase === CASES.ONE_PROJECT_ALL_GATEWAYS ? "gatewayId" : "projectId";
  const total = sums.length > 0 ? sums[sums.length - 1].totalSum : 0;
  sums.forEach((o, index) => {
    if (index !== sums.length - 1) {
      values.push(o.sum);
      labels.push(
        `${getNameByField(names, field, o.id)} ${Math.round(
          (o.sum * 100) / total
        )}%`
      );
    }
  });

  const data = {
    datasets: [
      {
        data: values,
        backgroundColor: ["#3F51B5", "#e53935", "#FB8C00"],
        borderWidth: 8,
        borderColor: "#FFFFFF",
        hoverBorderColor: "#FFFFFF",
      },
    ],
    labels,
  };
  return (
    <Box
      sx={{
        height: "100%",
        width: 521 * SCALE_WIDTH,
        backgroundColor: "white",
      }}
    >
      <Box>
        <Box
          sx={{
            height: 300,
          }}
        >
          <Doughnut style={{ height: "100%" }} data={data} options={options} />
          <Box
            style={{
              display: "flex",
              fontWeight: "bold",
              backgroundColor: "#F1FAFE",
              height: 53,
              alignItems: "center",
            }}
          >
            {selectedCase === CASES.ONE_PROJECT_ALL_GATEWAYS
              ? "PROJECT"
              : "GATEWAY"}{" "}
            Total | {total}
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

function sort(field, reports = [], sums = []) {
  const reportsWithSum = [];
  reports.sort((a, b) => {
    if (a[field] < b[field]) return -1;
    if (a[field] > b[field]) return 1;
    return 0;
  });
  let sum = 0;
  let lastValue = "";
  let lastIndex = 0;
  let totalSum = 0;
  reports.forEach((report, index) => {
    totalSum += report.amount;
    if (lastValue !== report[field] && lastValue !== "") {
      const id = reports[index - 1][field];
      reportsWithSum.splice(lastIndex, 0, { field, id, sum });
      sums.push({ field, id, sum });
      sum = report.amount;
      lastIndex = reportsWithSum.length;
    } else {
      sum += report.amount;
    }
    reportsWithSum.push(report);
    lastValue = report[field];
  });

  if (sum !== 0) {
    const id = reports[reports.length - 1][field];
    sums.push({ field, id, sum });
    reportsWithSum.splice(lastIndex, 0, { field, id, sum });
  }

  sums.push({ totalSum });
  reportsWithSum.push({ totalSum });
  return reportsWithSum;
}

function getNameByField(arr = [], field, value) {
  for (let e of arr) {
    if (e[field] === value) {
      return e.name;
    }
  }
  return "";
}
