import * as React from "react";
import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

const SCALE_WIDTH = screen.width/1440

export function BasicSelect({options = [], valueField='',labelField='',value,setValue, width}){
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  
  return (
    <Box style={{ backgroundColor:'#1BC5BD', borderRadius:5, }}>
      <FormControl fullWidth>
        <Select
          value={value}
          sx={{fontSize:14*SCALE_WIDTH, height:32*SCALE_WIDTH,width, color:'white'}}
          inputProps={{ 'aria-label': 'Without label' }}
          onChange={handleChange}
        >
          {options.map((option) => {
            return <MenuItem sx={{fontSize:14 }} value={option[valueField]}>{option[labelField]}</MenuItem>;
          })}
        </Select>
      </FormControl>
    </Box>
  );
}
