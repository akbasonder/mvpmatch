const API_URL = "http://178.63.13.157:8090/mock-api/api";

export const fetchUsers = () => {
  return fetch(`${API_URL}/users`, { method: "GET" });
};

export const fetchProjects = () => {
  return fetch(`${API_URL}/projects`, { method: "GET" });
};

export const fetchGateways = () => {
  return fetch(`${API_URL}/gateways`, { method: "GET" });
};

export const fetchReports = (from, to, projectId, gatewayId) => {
  return fetch(`${API_URL}/report`, {
    method: "POST",
    body: JSON.stringify({
      from,
      to,
      projectId,
      gatewayId,
    }),
    headers: {
        'Content-type': 'application/json; charset=UTF-8'
    },
  });
};
